package mobi.tikhonov.dblistviewtest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

public abstract class SingleFragmentActivity extends FragmentActivity {
	
	protected abstract Fragment createFragment();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView( R.layout.activity_fragment);	// root View element for each activity
													
		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
		
		if (fragment == null) {	//recreate only if no fragment was already preserved
			fragment =  createFragment();	// use class which will be implemented and returned by a child
			fm.beginTransaction()
			.add(R.id.fragmentContainer, fragment)
			.commit();
			}
	}

}
