package mobi.tikhonov.dblistviewtest;

import java.util.Locale;

import mobi.tikhonov.dblistviewtest.CountryLimitsContract.CountryLimit;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class DBMainListViewFragment extends ListFragment {

	TipserDBHelper db = null;
	
	private static final int ADD_COUNTRY = 0;
	private static final int UPDATE_COUNTRY = 1;
	
	ActionMode mActionMode = null;
	
	SharedPreferences mSharedPrefs;
	
	private PendingIntent pi = null;
	private AlarmManager mgr = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setup default preferences on the first app run (3rd false param)
		PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);
		
		mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mSharedPrefs.registerOnSharedPreferenceChangeListener(mPreferenceListener);
		
		Boolean testLogPref = mSharedPrefs.getBoolean("pref_test", false);
		if(testLogPref)
			Log.i("DBMainListViewFragment", "testLogPref is TRUE");
		else
			Log.i("DBMainListViewFragment", "testLogPref is FALSE");
		
		setHasOptionsMenu(true);
		setRetainInstance(true); 
			
		db = TipserDBHelper.getInstance(getActivity());
		new LoadCursorTask().execute();
	}
	
	@TargetApi(11)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		//View v = super.onCreateView(inflater, parent, savedInstanceState);
		View v = inflater.inflate(R.layout.dbmain_listview_fragment_layout, parent, false);
		
		EditText searchEditText = (EditText) v.findViewById(R.id.country_search_editText);
		
		searchEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				SimpleCursorAdapter adapter = (SimpleCursorAdapter) getListAdapter();
				
				 if (adapter!=null) {
					 adapter.getFilter().filter(s);
				 }
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
		
		searchEditText.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
					
					return true;
				}
				return false;
			}
			
		});
			
		ListView listView = (ListView)v.findViewById(android.R.id.list);
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {		// use context menu for old devices		
			registerForContextMenu(listView);
		} else {
			//listView.setLongClickable(true);	// do we need this? no. setOnItemLongClickListener sets it in long listener setup
			listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			
			listView.setMultiChoiceModeListener(new MultiChoiceModeListener() {

				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public void onDestroyActionMode(ActionMode mode) {
					// TODO Auto-generated method stub
				}

				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					MenuInflater inflater = mode.getMenuInflater();
					inflater.inflate(R.menu.delete_country_menu, menu);
					return true;
				}

				@Override
				public boolean onActionItemClicked(ActionMode mode,
						MenuItem item) {
					switch (item.getItemId()) {
					case R.id.menu_item_delete_country:
						SimpleCursorAdapter adapter = (SimpleCursorAdapter) getListAdapter();
						for (int i = adapter.getCount() - 1; i >= 0; i--) {
							if (getListView().isItemChecked(i)) {
								new DeleteTask().execute((long) getListView()
										.getItemIdAtPosition(i));
							}
						}
						mode.finish(); // Action picked, so
										// close the CAB
						return true;
					default:
						return false;
					}
				}

				@Override
				public void onItemCheckedStateChanged(ActionMode mode,
						int position, long id, boolean checked) {
					// TODO Auto-generated method stub
				}
			});
				
		}
		
		return v;
	}	
	
	@Override
	public void onResume() {
		super.onResume();

		IntentFilter filter = new IntentFilter(AlarmReceiver.BROADCAST);
		filter.setPriority(2);
		getActivity().registerReceiver(onNotice, filter);
	}
	
	@Override
	public void onPause() {
		super.onPause();

		getActivity().unregisterReceiver(onNotice);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Cursor record = ((CursorAdapter) getListAdapter()).getCursor();
		record.moveToPosition(position);
		
		String country = record.getString(record.getColumnIndex(CountryLimit.COLUMN_NAME_COUNTRY));
		Float limit = record.getFloat(record.getColumnIndex(CountryLimit.COLUMN_NAME_LIMIT));
		String country_code = record.getString(record.getColumnIndex(CountryLimit.COLUMN_NAME_COUNTRY_CODE));
		
		long record_id = id; //record.getInt(record.getColumnIndex(CountryLimit._ID));
		
		ContentValues countryInfo = new ContentValues();					
		countryInfo.put(CountryLimit.COLUMN_NAME_COUNTRY, country);
		countryInfo.put(CountryLimit.COLUMN_NAME_LIMIT, limit);
		countryInfo.put(CountryLimit.COLUMN_NAME_COUNTRY_CODE, country_code);
		DialogFragment addCountry = AddCountryDialogFragment.newInstance(countryInfo, record_id);
		
		addCountry.setTargetFragment(DBMainListViewFragment.this, UPDATE_COUNTRY);
		addCountry.show(getActivity().getSupportFragmentManager(),
				"AddCountryDialogFragment");
		
	}
	

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.dbmainlist_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_new_country:
			DialogFragment addCountry = AddCountryDialogFragment.newInstance(null, -1);	//passing no data
			addCountry.setTargetFragment(DBMainListViewFragment.this, ADD_COUNTRY);		// DialogFragment will be empty
			addCountry.show(getActivity().getSupportFragmentManager(),					// will return country data w/o _id
					"AddCountryDialogFragment");
			return (true);
		case R.id.menu_item_statistics:
			startActivity(new Intent(getActivity(), StatisticsActivity.class));
			return true;
		case R.id.menu_item_settings:
			//Intent i = new Intent(getActivity(), SettingsActivity.class);
			startActivity(new Intent(getActivity(), SettingsActivity.class));
			return true;
		case R.id.menu_item_about:
			String url = "http://tikhonov.mobi";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);
			return true;
		case R.id.menu_item_notification_start:
			AlarmReceiver.scheduleAlarms(getActivity());
			return true;
		case R.id.menu_item_notification_stop:
			AlarmReceiver.stopAlarms(getActivity());
			return true;
		case R.id.check_current_country:
			checkCurrentCountry();
			return true;
		}

		return (super.onOptionsItemSelected(item));
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		getActivity().getMenuInflater().inflate(R.menu.delete_country_menu, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
	    
	    switch (item.getItemId()) {
	        case R.id.menu_item_delete_country:
	        	new DeleteTask().execute(info.id);
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent i) {
		if (resultCode != Activity.RESULT_OK)
			return;

		if (requestCode == ADD_COUNTRY) {
			ContentValues countryInfo = (ContentValues) i.getParcelableExtra(AddCountryDialogFragment.COUNTRY_INFO);
			new InsertTask().execute(countryInfo);
		} else if (requestCode == UPDATE_COUNTRY) {
			ContentValues countryInfo = (ContentValues) i.getParcelableExtra(AddCountryDialogFragment.COUNTRY_INFO);
			long id = i.getLongExtra(AddCountryDialogFragment.COUNTRY_ID, -1);
			new UpdateTask(countryInfo, id).execute();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		((CursorAdapter) getListAdapter()).getCursor().close();
		//db.close();	//some other activities of threads might be working with db!
	}
	
	private class LoadCursorTask extends AsyncTask<Void, Void, Void> {
		
		private Cursor local_cursor = null;
		
		@Override
		protected Void doInBackground(Void... params) {
			//SQLiteDatabase local_db = db.getWritableDatabase();

			local_cursor = getRWDB();
			local_cursor.getCount();

			return (null);
		}

		@SuppressWarnings("deprecation")
		@Override
		public void onPostExecute(Void arg0) {

			SimpleCursorAdapter adapter;

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				adapter = new SimpleCursorAdapter(getActivity(),
						R.layout.country_limits_listview_layout,
						local_cursor, new String[] {
								CountryLimit.COLUMN_NAME_COUNTRY,
								CountryLimit.COLUMN_NAME_LIMIT,
								CountryLimit.COLUMN_NAME_COUNTRY_CODE },

						new int[] { R.id.coutry_limits_countryNametextView,
								R.id.coutry_limits_countryLimittextView,
								R.id.coutry_limits_countryCodetextView }, 0);
			} else {
				adapter = new SimpleCursorAdapter(getActivity(),
						R.layout.country_limits_listview_layout,
						local_cursor, new String[] {
								CountryLimit.COLUMN_NAME_COUNTRY,
								CountryLimit.COLUMN_NAME_LIMIT,
								CountryLimit.COLUMN_NAME_COUNTRY_CODE },

						new int[] { R.id.coutry_limits_countryNametextView,
								R.id.coutry_limits_countryLimittextView,
								R.id.coutry_limits_countryCodetextView });
			}

			setListAdapter(adapter);
			// wire up filter interface callback implementation
			adapter.setFilterQueryProvider(new FilterQueryProvider() {
				
				@Override
				public Cursor runQuery(CharSequence constraint) {
					return db.getWritableDatabase().rawQuery(
							"SELECT " + CountryLimit._ID + ", "
									+ CountryLimit.COLUMN_NAME_COUNTRY + ", "
									+ CountryLimit.COLUMN_NAME_LIMIT + ", "
									+ CountryLimit.COLUMN_NAME_COUNTRY_CODE + " "
									+ " FROM " + CountryLimit.TABLE_NAME + " "
									+ CountryLimit.TABLE_NAME + " " 
									+ "WHERE " + CountryLimit.COLUMN_NAME_COUNTRY + " "
									+ "LIKE " + "'" + constraint + "%'" + " "
									+ "ORDER BY "
									+ CountryLimit.COLUMN_NAME_COUNTRY + " "
									+ "COLLATE NOCASE;", null);
				}
			});
		}
	}
	
	private class InsertTask extends AsyncTask<ContentValues, Void, Void> {
		private Cursor local_cursor = null;

		@Override
		protected Void doInBackground(ContentValues... values) {
			db.getWritableDatabase().insert(CountryLimit.TABLE_NAME,
					CountryLimit.COLUMN_NAME_COUNTRY, values[0]);

			local_cursor = getRWDB();		
			local_cursor.getCount();
			return (null);
		}

		@Override
		public void onPostExecute(Void arg0) {
			((CursorAdapter) getListAdapter()).changeCursor(local_cursor);
		}
	}
	
	private class UpdateTask extends AsyncTask<Void, Void, Void> {
		private Cursor local_cursor = null;
		private long record_id = 0;
		private ContentValues country_values = null;
		
		UpdateTask(ContentValues values, long id) {
			this.country_values = values;
			this.record_id = id;
		}

		@Override
		protected Void doInBackground(Void... values) {

			if (record_id != -1) {
				db.getWritableDatabase().update(CountryLimit.TABLE_NAME,
						country_values, CountryLimit._ID + " = " + record_id,
						null);
			}

			local_cursor = getRWDB();
			local_cursor.getCount();
			return (null);
		}

		@Override
		public void onPostExecute(Void arg0) {
			((CursorAdapter) getListAdapter()).changeCursor(local_cursor);
		}
	}
	
	private class DeleteTask extends AsyncTask<Long, Void, Void> {
		private Cursor local_cursor = null;

		@Override
		protected Void doInBackground(Long... values) {
			db.getWritableDatabase().delete(CountryLimit.TABLE_NAME, CountryLimit._ID + "=" + values[0], null); 

			local_cursor = getRWDB();		
			local_cursor.getCount();
			return (null);
		}

		@Override
		public void onPostExecute(Void arg0) {
			((CursorAdapter) getListAdapter()).changeCursor(local_cursor);
		}
	}
	
	private Cursor getRWDB() {

		return db.getWritableDatabase().rawQuery(
				"SELECT " + CountryLimit._ID + ", "
						+ CountryLimit.COLUMN_NAME_COUNTRY + ", "
						+ CountryLimit.COLUMN_NAME_LIMIT + ", "
						+ CountryLimit.COLUMN_NAME_COUNTRY_CODE + " "
						+ " FROM " //+ CountryLimit.TABLE_NAME + " "
						+ CountryLimit.TABLE_NAME + " " + "ORDER BY "
						+ CountryLimit.COLUMN_NAME_COUNTRY + " "
						+ "COLLATE NOCASE;", null);
	}
	
	 OnSharedPreferenceChangeListener mPreferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
	      public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
	    	  // will this always work? This activity/fragment goes to background...will it be GCed?
	    	  // perhaps, the strategy is to initialize some instance variables and use them in onCreate onCreateView
	    	  // rather than manipulate interface since this activity will be replaces with PreferenceActivity anyway
	    	  // and will thus undergo lifecycle change
	    	  Toast.makeText(getActivity().getApplicationContext(), "Shared prefs were changed", Toast.LENGTH_LONG).show();
	    	  
	    	  if(key.equals("pref_notification_timeout")) {// should not start alarm if it's not started
	    		  AlarmReceiver.scheduleAlarms(getActivity());
	    		  Log.d(getClass().getSimpleName(), "Preference has changed. Restarting Alarm");
	    	  }
	      }
	    };
	    
	private BroadcastReceiver onNotice = new BroadcastReceiver() {
		public void onReceive(Context ctxt, Intent i) {
			Toast.makeText(getActivity().getApplicationContext(), "Alarm", Toast.LENGTH_SHORT).show();
			abortBroadcast();
		}
	};
	
	/*
	 * Offline current country check. 
	 *  Tries to get current country via GSM (only!) network
	 *  Reverts to Locale settings (depends on the language).
	 */
	private void checkCurrentCountry() {
		String countryCode = null;

		TelephonyManager telMgr = (TelephonyManager) getActivity()
				.getSystemService(Context.TELEPHONY_SERVICE);

		if (telMgr.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) {
			countryCode = telMgr.getNetworkCountryIso();

			if (!countryCode.equals("")) {
				Toast.makeText(getActivity().getApplicationContext(),
						"Network: " + countryCode.toLowerCase(), Toast.LENGTH_SHORT).show();
				return;
			}

			Locale locale = Locale.getDefault();
			countryCode = locale.getCountry();

			Toast.makeText(getActivity().getApplicationContext(),
					"Locale: " + countryCode.toLowerCase(), Toast.LENGTH_SHORT).show();
		}
	}

}
