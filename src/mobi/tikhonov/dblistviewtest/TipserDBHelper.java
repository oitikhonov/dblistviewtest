package mobi.tikhonov.dblistviewtest;

import mobi.tikhonov.dblistviewtest.CountryLimitsContract.CountryLimit;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TipserDBHelper extends SQLiteOpenHelper {
	
	private static TipserDBHelper sDBHelper = null;
	
	private Context mContext;	// not sure if it'll ever be needed. let's save in just in case though.
	
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Tipser.db";
    
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " NUMERIC";
    private static final String COMMA_SEP = ", ";
    private static final String SQL_CREATE_ENTRIES =
        "CREATE TABLE " + CountryLimit.TABLE_NAME + " (" +
        CountryLimit._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        CountryLimit.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
        CountryLimit.COLUMN_NAME_LIMIT+ INTEGER_TYPE + COMMA_SEP +
        CountryLimit.COLUMN_NAME_COUNTRY_CODE + TEXT_TYPE +
        " );";

    private static final String SQL_DELETE_ENTRIES =
        "DROP TABLE IF EXISTS " + CountryLimit.TABLE_NAME;
    
    
    
    private TipserDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        
        mContext = context;
    }
    
    public static TipserDBHelper getInstance(Context c) {
		if (sDBHelper == null) {
			sDBHelper = new TipserDBHelper(c.getApplicationContext());
		}
		return sDBHelper;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		try {			
			db.beginTransaction();
			
			db.execSQL(SQL_CREATE_ENTRIES);
			
			ContentValues cv= new ContentValues();
			long roid = 0;
			
			cv.put(CountryLimit.COLUMN_NAME_COUNTRY, "Russia");
			cv.put(CountryLimit.COLUMN_NAME_LIMIT, 0.03);
			cv.put(CountryLimit.COLUMN_NAME_COUNTRY_CODE, "RU");
			roid = db.insert(CountryLimit.TABLE_NAME, CountryLimit.COLUMN_NAME_COUNTRY, cv);
			Log.d("ROW ID #", Long.toString(roid));
			
			cv.put(CountryLimit.COLUMN_NAME_COUNTRY, "USA");
			cv.put(CountryLimit.COLUMN_NAME_LIMIT, 0.03);
			cv.put(CountryLimit.COLUMN_NAME_COUNTRY_CODE, "US");
			roid = db.insert(CountryLimit.TABLE_NAME, CountryLimit.COLUMN_NAME_COUNTRY, cv);
			Log.d("ROW ID #", Long.toString(roid));
			
			cv.put(CountryLimit.COLUMN_NAME_COUNTRY, "Austrlia");
			cv.put(CountryLimit.COLUMN_NAME_LIMIT, 0.05);
			cv.put(CountryLimit.COLUMN_NAME_COUNTRY_CODE, "AU");
			roid = db.insert(CountryLimit.TABLE_NAME, CountryLimit.COLUMN_NAME_COUNTRY, cv);
			Log.d("ROW ID #", Long.toString(roid));
			
			db.setTransactionSuccessful();
			}
		finally {
			db.endTransaction();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 db.execSQL(SQL_DELETE_ENTRIES);
	     onCreate(db);
	}

}
