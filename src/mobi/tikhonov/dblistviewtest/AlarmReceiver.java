package mobi.tikhonov.dblistviewtest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;

public class AlarmReceiver extends BroadcastReceiver {
	
	public static final String BROADCAST =
		    "mobi.tikhonov.dblistviewtest.AlarmReceiver.COUNTRY_NOTIFICATION";
	private static Intent broadcast = new Intent(BROADCAST);

	@Override
	public void onReceive(Context context, Intent intent) {
		if(intent.getAction() == null) {
			context.sendOrderedBroadcast(broadcast, null);
		}else if (intent.getAction().equals("mobi.tikhonov.dblistviewtest.AlarmReceiver.COUNTRY_NOTIFICATION")) {	// alarm fired up
			WakefulIntentService.sendWakefulWork(context, NotificationService.class);
		} else if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {	//implicit intent from ACTION_BOOT_COMPLETED from Manifest
			scheduleAlarms(context);
		}
	}

	static void scheduleAlarms(Context context) {
		AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, AlarmReceiver.class);	//we're calling ourselves upon alarm timeout by getting explicit intent
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);

		SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		int PERIOD = Integer.parseInt(mSharedPrefs.getString("pref_notification_timeout", "10")) * 1000; // option is stored in Seconds
				
		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 5000, PERIOD, pi);
		
		Log.d("scheduleAlarms", "Starting Alarm");

	}
	
	static void stopAlarms(Context context) {
		AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, AlarmReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		
		mgr.cancel(pi);
		
		Log.d("scheduleAlarms", "Stopping Alarm");
	}
}
