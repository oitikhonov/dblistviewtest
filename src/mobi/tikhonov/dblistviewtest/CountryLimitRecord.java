package mobi.tikhonov.dblistviewtest;

public class CountryLimitRecord {
	
	String country;
	Float limit;
	String country_code;
	
	CountryLimitRecord(String c, Float l, String cc) {
		
		country = c;
		limit = l;
		country_code = cc;
		
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String c) {
		country = c;
	}
	
	public String getCountryCode() {
		return country_code;
	}
	
	public void setCountryCode(String c) {
		country_code = c;
	}
	
	public Float getLimit() {
		return limit;
	}
	
	public void setLimit(Float l) {
		limit = l;
	}
	
	public String toString() {
		return country;
	}

}
