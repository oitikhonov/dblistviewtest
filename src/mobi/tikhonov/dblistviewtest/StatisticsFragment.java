package mobi.tikhonov.dblistviewtest;

import java.util.ArrayList;

import mobi.tikhonov.dblistviewtest.CountryLimitsContract.CountryLimit;
import android.annotation.TargetApi;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.BarGraph.OnBarClickedListener;
import com.echo.holographlibrary.Line;
import com.echo.holographlibrary.LineGraph;
import com.echo.holographlibrary.LineGraph.OnPointClickedListener;
import com.echo.holographlibrary.LinePoint;
import com.echo.holographlibrary.PieGraph;
import com.echo.holographlibrary.PieGraph.OnSliceClickedListener;
import com.echo.holographlibrary.PieSlice;

public class StatisticsFragment extends Fragment {
	
	private TipserDBHelper db = null;
	private BarGraph country_bg;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		
		db = TipserDBHelper.getInstance(getActivity());
		new LoadCursorTask().execute();
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.statistics_layout, parent, false);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		
		country_bg = (BarGraph)v.findViewById(R.id.country_bargraph);
		
		/*
		 * generic graphs for
		 * test purposes follow
		 */
		
		PieGraph pg = (PieGraph)v.findViewById(R.id.piegraph);
		PieSlice slice = new PieSlice();
		slice.setColor(Color.parseColor("#99CC00"));
		slice.setValue(50);
		pg.addSlice(slice);
		slice = new PieSlice();
		slice.setColor(Color.parseColor("#FFBB33"));
		slice.setValue(49);
		pg.addSlice(slice);
		slice = new PieSlice();
		slice.setColor(Color.parseColor("#AA66CC"));
		slice.setValue(1);
		pg.addSlice(slice);		
		
		pg.setOnSliceClickedListener(new OnSliceClickedListener(){

			@Override
			public void onClick(int index) {
				Log.i("StatisticsFragment", "PIE index = " + index);
			}
			
		});
		
		Line l = new Line();
		
		LinePoint p = new LinePoint();
		p.setX(0);
		p.setY(0);
		l.addPoint(p);
		
		p = new LinePoint();
		p.setX(0);
		p.setY(5);
		l.addPoint(p);
		
		p = new LinePoint();
		p.setX(5);
		p.setY(5);
		l.addPoint(p);
		
		p = new LinePoint();
		p.setX(10);
		p.setY(10);
		l.addPoint(p);
		
		l.setColor(Color.parseColor("#FFBB33"));
		
		Line l2 = new Line();
		
		p = new LinePoint();
		p.setX(0);
		p.setY(6);
		l2.addPoint(p);
		
		p = new LinePoint();
		p.setX(6);
		p.setY(6);
		l2.addPoint(p);
		
		l2.setColor(Color.parseColor("#66BB33"));

		LineGraph li = (LineGraph) v.findViewById(R.id.linegraph);
		li.addLine(l);
		li.setRangeY(0, 10);
		li.setLineToFill(0);
		
		li.addLine(l2);
		li.setRangeY(0, 10);
		li.setLineToFill(0);
		
		li.setOnPointClickedListener(new OnPointClickedListener() {
			
			@Override
			public void onClick(int lineIndex, int pointIndex) {
				Log.i("StatisticsFragment", "Line index = " + lineIndex + " pointIndex = " + pointIndex);
				
			}
		});
		
		ArrayList<Bar> points = new ArrayList<Bar>();
		Bar d = new Bar();
		d.setColor(Color.parseColor("#99CC00"));
		d.setName("Test1");
		d.setValue(10);
		Bar d2 = new Bar();
		d2.setColor(Color.parseColor("#FFBB33"));
		d2.setName("Test2");
		d2.setValue(20);
		points.add(d);
		points.add(d2);

		BarGraph g = (BarGraph)v.findViewById(R.id.bargraph);
		g.setBars(points);
	
		g.setOnBarClickedListener(new OnBarClickedListener() {
			
			@Override
			public void onClick(int index) {
				Log.i("StatisticsFragment", "BAR index = " + index);
				
			}
		});
		
		return v;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(getActivity());
			return (true);
		}

		return (super.onOptionsItemSelected(item));
	}
	
	
	private class LoadCursorTask extends AsyncTask<Void, Void, Void> {

		private Cursor local_cursor = null;

		@Override
		protected Void doInBackground(Void... params) {
			// SQLiteDatabase local_db = db.getWritableDatabase();

			local_cursor = db.getWritableDatabase().rawQuery(
					"SELECT " + CountryLimit.COLUMN_NAME_COUNTRY + ", "
							+ CountryLimit.COLUMN_NAME_LIMIT + " "
							+ " FROM " + CountryLimit.TABLE_NAME + " "
							+ " WHERE " + CountryLimit.COLUMN_NAME_LIMIT + " > 10;", null);	//10 makes testing easier

			local_cursor.getCount();

			return (null);
		}

		@Override
		public void onPostExecute(Void arg0) {
			//if possible, put a non-blocking pause. then update graph for cool visual effect
			
			
			//slice.setTitle(Integer.toString(local_cursor.getCount()));
			
			ArrayList<Bar> points = new ArrayList<Bar>();
			Bar d = new Bar();
			d.setColor(Color.parseColor("#99CC00"));
			d.setName("Limit > 10");
			d.setValue(local_cursor.getCount());
			
			Bar d2 = new Bar();
			d2.setColor(Color.parseColor("#FFBB33"));
			d2.setName("Test2");
			d2.setValue(10);
			
			//if(local_cursor.getCount() != 0)
				points.add(d);
			
			points.add(d2);

			country_bg.setBars(points);

		}
	}

}
