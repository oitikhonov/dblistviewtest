package mobi.tikhonov.dblistviewtest;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.Menu;

public class DBMainListView extends SingleFragmentActivity {

	protected Fragment createFragment() {

		return new DBMainListViewFragment();
	}

}
