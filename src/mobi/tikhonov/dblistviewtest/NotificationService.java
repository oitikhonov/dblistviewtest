package mobi.tikhonov.dblistviewtest;

import mobi.tikhonov.dblistviewtest.CountryLimitsContract.CountryLimit;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;

public class NotificationService extends WakefulIntentService {
	
	 private static int COUNTRY_NOTIFY_ID = 0;	// must be static as the service restart. alternatively - save in preferences!
	 											// static? will it be GC'ed?

	public NotificationService() {
		super("NotificationService");
	}

	@Override
	protected void doWakefulWork(Intent arg0) {
		// can't make Toast because GUI is not available from here?		
		// we are in worker thread, use DB to get some data and present it via log or Notification
		
		Log.d(getClass().getSimpleName(), "I ran!");
				
		NotificationCompat.Builder b = new NotificationCompat.Builder(this);
		b.setAutoCancel(true)
		.setDefaults(Notification.DEFAULT_LIGHTS)
		.setWhen(System.currentTimeMillis());
		
		String info = getCountryData(COUNTRY_NOTIFY_ID);
		
		if(info.equals("")) {
			COUNTRY_NOTIFY_ID = 0;
			info = getCountryData(COUNTRY_NOTIFY_ID);
		}
		
		b.setContentTitle("#" + COUNTRY_NOTIFY_ID)
		.setContentText(info)
		.setSmallIcon(android.R.drawable.stat_sys_warning)
		.setTicker("country update");
		
		b.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, DBMainListView.class), 0));
		
		NotificationManager mgr = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		
		mgr.notify(COUNTRY_NOTIFY_ID++, b.build());
	}
	
	String getCountryData(int country_id) {	// make sure it checks the boundary?
		
		TipserDBHelper db = TipserDBHelper.getInstance(this);	//move this and cursor to constructor later? what if data changes? 
		
		Cursor local_cursor = db.getWritableDatabase().rawQuery(
				"SELECT " + CountryLimit.COLUMN_NAME_COUNTRY + ", "
						+ CountryLimit.COLUMN_NAME_LIMIT + " "
						+ " FROM " + CountryLimit.TABLE_NAME + " "
						+ "ORDER BY " + CountryLimit.COLUMN_NAME_COUNTRY + " "
						+ "COLLATE NOCASE;", null);
						//+ " WHERE " + CountryLimit._ID + " = " + country_id + ";", null);

		local_cursor.moveToFirst();
		Log.d(getClass().getSimpleName(), local_cursor.toString());
		
		if(local_cursor.getCount() != 0) {
			if(local_cursor.move(country_id)) {
				String country = local_cursor.getString(local_cursor.getColumnIndex(CountryLimit.COLUMN_NAME_COUNTRY));
				Float limit = local_cursor.getFloat(local_cursor.getColumnIndex(CountryLimit.COLUMN_NAME_LIMIT));
				
				return "Name: " + country + " Limit: " + limit;
			}
		}
		
		return ""; //lame fail recovery
	}

}
