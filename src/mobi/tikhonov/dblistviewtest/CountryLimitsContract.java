package mobi.tikhonov.dblistviewtest;

import android.provider.BaseColumns;

public final class CountryLimitsContract {

	// To prevent someone from accidentally instantiating the contract class,
	// give it an empty constructor.
	public CountryLimitsContract() {
	}

	/* Inner class that defines the table contents */
	public static abstract class CountryLimit implements BaseColumns {
		public static final String TABLE_NAME = "countrylimit";
		public static final String COLUMN_NAME_COUNTRY = "country";
		public static final String COLUMN_NAME_LIMIT = "alco_limit";
		public static final String COLUMN_NAME_COUNTRY_CODE = "country_code";
	}

}
