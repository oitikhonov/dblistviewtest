package mobi.tikhonov.dblistviewtest;

import mobi.tikhonov.dblistviewtest.CountryLimitsContract.CountryLimit;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class AddCountryDialogFragment extends DialogFragment {
	
	public static final String COUNTRY_INFO = "mobi.tikhonov.dblistviewtest.country_info";
	public static final String COUNTRY_ID = "mobi.tikhonov.dblistviewtest.country_id";
	
	TextView countryName;
	TextView countryLimit;
	TextView countryCode;
	
	long record_id;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
				
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.countrydialog_layout, null);
		
		countryName = (TextView) dialogView.findViewById(R.id.country_nameeditText);
		countryLimit = (TextView) dialogView.findViewById(R.id.country_limitEditText);
		countryCode = (TextView) dialogView.findViewById(R.id.country_codeEditText);

		
		
		if (getArguments() != null) {
			ContentValues countryInfo = (ContentValues) getArguments()
					.getParcelable(COUNTRY_INFO);
			record_id =  getArguments().getLong(COUNTRY_ID);

			if (countryInfo != null) {		
				countryName.setText(countryInfo.getAsString(CountryLimit.COLUMN_NAME_COUNTRY));
				countryLimit.setText(countryInfo.getAsString(CountryLimit.COLUMN_NAME_LIMIT));
				countryCode.setText(countryInfo.getAsString(CountryLimit.COLUMN_NAME_COUNTRY_CODE));
			}
		}
		
		builder.setTitle(R.string.new_country)
				.setView(dialogView)
				.setPositiveButton(R.string.dialog_ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								if (!countryName.getText().toString().equals("") &&
									!countryLimit.getText().toString().equals("") &&
									!countryCode.getText().toString().equals("")) {
									
									ContentValues countryInfo = new ContentValues();
									countryInfo.put(CountryLimit.COLUMN_NAME_COUNTRY, countryName.getText().toString());
									countryInfo.put(CountryLimit.COLUMN_NAME_LIMIT, countryLimit.getText().toString());
									countryInfo.put(CountryLimit.COLUMN_NAME_COUNTRY_CODE, countryCode.getText().toString());
									
									if (getTargetFragment() == null)
										return;
										
										Intent i = new Intent();
										i.putExtra(COUNTRY_INFO, countryInfo);
										i.putExtra(COUNTRY_ID, record_id);
										getTargetFragment()
										.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
								}
							}
						})
				.setNegativeButton(R.string.dialog_cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// CANCEL
							}
						});
		// Create the AlertDialog object and return it
		return builder.create();
	}
	
	public static AddCountryDialogFragment newInstance(ContentValues countryInfo, long id) {
		AddCountryDialogFragment fragment = new AddCountryDialogFragment();
		
		if (countryInfo != null) {
			Bundle args = new Bundle();
			args.putParcelable(COUNTRY_INFO, countryInfo);
			args.putLong(COUNTRY_ID, id);
			fragment.setArguments(args);
		}
		
		return fragment;
	}

}
