This is not a real project. I hack stuff which I'll need for the real one.

Implemented so far:

- Custom ListView backed up by SQLite
- Everything (I hope so) is done via separate threads to make the app fast
- ListView Filters
- Item Addition/Updating/Deletion via ugly custom Dialog (should be redone via proper Fragment)
- Action mode operations for new Android versions, plain old menus for older versions
- SharedOptions Activity
- Async soft-locking background Service to produce test Notification (yes, in the bar)
- No notification if there is an Activity in the foreground
- Test graphs to show statistics (bars, pie)
- About menu options to send to my personal web page
- Notification tap opens the app
- Offline current country identification (from GSM networks, then - revert to Locale country info)

TODO:

- Abandon the whole thing and proceed with the real app